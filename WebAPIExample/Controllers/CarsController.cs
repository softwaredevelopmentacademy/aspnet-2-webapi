﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Web.ModelBinding;

namespace WebAPIExample.Controllers
{
    [Route("/api/v3/cars")]
    public class CarsController : ApiController
    {
        private static readonly List<Car> Cars = new List<Car>
        {
            new Car { Id = 1, Name = "Subaru" },
            new Car { Id = 2, Name = "Mercedes" },
            new Car { Id = 3, Name = "Smart" }
        };

        // Example of query string mixed with url parameter
        // ~/api/v3/cars/5?name="subaru"
        [Route("{id}")]
        public IEnumerable<Car> GetByIdAndName(int id, [QueryString]string name)
        {
            return Cars;
        }

        public IEnumerable<Car> Get()
        {
            return Cars;
        }

        [Route("{id}")]
        public Car Get(int id)
        {
            return Cars.Single(item => item.Id == id);
        }

        // POST: api/Cars
        public void Post([FromBody]Car value)
        {
            Cars.Add(value);
            string urlToApiToGetThisCar = new UrlHelper()
                .Route("CarsGet", new {id = value.Id});

            SendEmail(urlToApiToGetThisCar);
        }

        private void SendEmail(object urlToApiToGetThisCar)
        {
            throw new System.NotImplementedException();
        }

        // PUT: api/Cars/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Cars/5
        public void Delete(int id)
        {
        }
    }
}
