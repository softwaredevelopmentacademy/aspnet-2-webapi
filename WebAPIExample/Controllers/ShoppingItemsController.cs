﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebAPIExample.Controllers
{
    public class ShoppingItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
    }

    public class ShoppingItemsController : ApiController
    {
        private static readonly List<ShoppingItem> ShoppingItems = new List<ShoppingItem>
        {
            new ShoppingItem { Id = 1, Name = "First shopping item", Price = 100 },
            new ShoppingItem { Id = 2, Name = "Second shopping item", Price = 200 },
            new ShoppingItem { Id = 3, Name = "Thirdth shopping item", Price = 300 }
        };

        public IEnumerable<ShoppingItem> Get()
        {
            return ShoppingItems;
        }

        public ShoppingItem Get(int id)
        {
            return ShoppingItems.Single(item => item.Id == id);
        }

        // POST: api/ShoppingItems
        public void Post([FromBody]ShoppingItem value)
        {
            ShoppingItems.Add(value);
        }

        // PUT: api/ShoppingItems/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ShoppingItems/5
        public void Delete(int id)
        {
        }
    }
}
